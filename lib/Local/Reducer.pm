package Local::Reducer;

use strict;
use warnings;

=encoding utf8

=head1 NAME

Local::Reducer - base abstract reducer

=head1 VERSION

Version 1.00

=cut

our $VERSION = '1.00';

=head1 SYNOPSIS

=cut

sub new {
    my ( $class, %params ) = @_;
    return bless \%params, $class;
}

sub reduce_n {
    my ($self, $n) = @_;
    my $reduced_value = $self->{initial_value};
    while ( $n-- and my $str = $self->{source}->next ) {
        $reduced_value = $self->internal_reduce( $str, $self->{row_class}->new(str => $str), $reduced_value );
    }
    return $self->{reduced} = $reduced_value;
}

sub reduce_all {
    my $self          = shift;
    my $reduced_value = $self->{initial_value};
    $self->{source}->reset_iterator;
    while ( my $str = $self->{source}->next ) {
        $reduced_value = $self->internal_reduce( $str, $self->{row_class}->new(str => $str), $reduced_value );
    }
    return $self->{reduced} = $reduced_value;
}

sub reduced {
    my $self = shift;
    return $self->{reduced};
}

1;
