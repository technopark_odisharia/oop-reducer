package Local::Reducer::MaxDiff;

use strict;
use warnings;

# debug
use DDP;

use parent qw(Local::Reducer);

sub internal_reduce {
    my ($self, $str, $row, $value) = (@_, 0);
    my ($top, $bottom) = ( $row->get($self->{top}, undef),
                           $row->get($self->{bottom}, undef) );
    return $value = abs($top - $bottom) if $value < abs($top - $bottom);
}

1;
