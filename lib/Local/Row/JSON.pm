package Local::Row::JSON;

use strict;
use warnings;
use JSON::XS;

use parent qw(Local::Row);

# debug
use DDP;

sub get {
    my ($self, $name, $default, $field) = (@_, []);
    $self->analyse_JSON_tree_recursively($name, decode_json($self->{str}), $field);
    if ( @{$field} ) {
        ( scalar @{$field} == 1 )
        ? ( return $field->[0] )
        : ( return $field )
    }
    else {
        return $default
    }
}

# TODO Add to get as recursive closure if it's possible.
# Idk how to implement it's right now.
# Maybe closure in closure? Idk.
sub analyse_JSON_tree_recursively {
    my ($self, $name, $json) = @_;
    my @field;

    my $JSON_search_field;
    $JSON_search_field = sub {
        my $json = shift;
        if ( ref($json) eq 'HASH' ) {
            foreach ( keys %{$json} ) {
                if ( ref $json->{$_} eq 'HASH' ) {
                    $JSON_search_field->($json->{$_});
                }
                else {
                    push(@field, $json->{$name}) if ( $json->{$name} );
                }
            }
        } elsif ( ref $json eq 'ARRAY' ) {
            foreach ( @{$json} ) {
                ( ref($_) eq 'HASH' )
                ? ( $JSON_search_field->($_) )
                : ( next )
            }
        }
    }



    return @field;
}

1;
