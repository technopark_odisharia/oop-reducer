package Local::Reducer::Sum;

use strict;
use warnings;

# debug
use DDP;

use parent qw(Local::Reducer);

sub internal_reduce {
    my ($self, $str, $row, $value) = @_;
    return $value += $row->get($self->{field}, undef);
}

1;
